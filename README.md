# Tutorial Django + Mongodb (CI/CD)

This repo contains code to deploy a Django app with Mongodb database using CI/CD and Dockirization using Dockerfile and Docker-Compose.

## Introduction

GitLab CI/CD is a powerful tool for automating the software development process, from building and testing code to deploying it to various environments. One of the many options for deploying code is to use GCP's App Engine, a fully managed platform for building and deploying web applications. In this article, we will show you how to set up a GitLab CI/CD pipeline to deploy a simple web application to App Engine, using the `CI_JOB_JWT_V2` token for authentication.

## Project setup

```
|---- quickstart
      |---- migrations
      |---- __init__.py
      |---- admin.py
      |---- apps.py
      |---- models.py
      |---- tests.py
      |---- views.py
|---- tutorial
      |---- __pycache__
      |---- __init__.py
      |---- asgi.py
      |---- settings.py
      |---- urls.py
      |---- wsgi.py
|---- .gitlab-ci.yml
|---- docker-compose.yml
|---- Dockerfile
|---- manager.py
|---- requirements.txt

```

## Dockerfile
Docker can build images automatically by reading the instructions from a Dockerfile. A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image. This page describes the commands you can use in a Dockerfile.
```yml
FROM python:3.9.1-alpine

RUN apk add --update --no-cache \
    g++ gcc libxslt-dev musl-dev python3-dev \
    libffi-dev openssl-dev jpeg-dev zlib-dev

ENV LIBRARY_PATH=/lib:/usr/lib

# add and change to non-root user in image
ENV HOME=/home/worker
RUN adduser --home ${HOME} --disabled-password worker
RUN chown worker:worker ${HOME}
USER worker

# Set the working directory for any following RUN, CMD, ENTRYPOINT, COPY and ADD instructions
WORKDIR ${HOME}


COPY ./requirements.txt ${HOME}

RUN pip install --upgrade pip

RUN pip install --no-cache-dir -r requirements.txt

COPY . ${HOME}

```


## docker-compose.yml

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.
```yml
version: "3.2"

services:

  mongodby:
         image: mongo
         command: mongod --port 27017
         volumes:
            - backend_db:/data/db
         restart: "always"
         environment: 
           - MONGO_INITDB_DATABASE=DBIndexer
           - MONGO_INITDB_ROOT_USERNAME=root
           - MONGO_INITDB_ROOT_PASSWORD=mongoadmin
         
         ports:
           - "27017:27017"

  django:
    container_name: django
    restart: always
    build: ./
    ports:
      - "8000:8000"
    links:
      - mongodby
    depends_on:
      - mongodby

    command: >
      sh -c " sleep 20 &&
      python manage.py makemigrations &&
      python manage.py migrate &&
      python manage.py runserver  0.0.0.0:8000"

    volumes:
      - media:/usr/src/app
      
volumes:
  backend_db:
  media:
  static:


```

## .gitlab-ci.yml

GitLab CI (Continuous Integration) service is a part of GitLab that build and test the software whenever developer pushes code to application. GitLab CD (Continuous Deployment) is a software service that places the changes of every code in the production which results in every day deployment of production.

<h3>The Container Registry</h3>
When docker images are built in CI/CD pipeline they need to be pushed somewhere, where they are stored and from where they can be accessed again. This location is the so-called registry. Gitlab offers such a location in each project. This is the so-called container registry.
<br/><br/>
The container registry can be found under <b>Menu > Packages & Registries > Container Registry</b>
<br/><br/>
By default, the container registry is enabled for project under <b>Menu > Settings > General > Visibility, project features, permissions > Container Registry</b>
<br/><br/>
In CI/CD jobs the container registry can be accessed via
<ul>
<li><code>$CI_REGISTRY</code>. This is the URL to the GitLab registry server:
<br/>registry.gitlab.com</li>
<li><code>$CI_REGISTRY_IMAGE</code>. This is the URL to the GitLab registry of the project, which is for this project: 
<br/>registry.gitlab.com/topfreelance/kurt/back_end/example-app</li>
</ul>
After each successful run of a CI/CD pipeline a new docker image is stored in the container registry. For this reason, the used space grows over time. GitLab offers a feature to easily clean-up old images, which are not needed anymore. This feature is disabled by default. In this project is enabled and configured to do a daily clean-up and to store only the last 5 images. This can be done under <b>Menu > Settings > Packages & Registries > Container Registry</b>. 
<br/><br/>
To secure the access to the container registry, a deploy token needs to be created.

<br/>
<br/>
<h3>The Deploy Token</h3>
As a first step, a deploy token was created under: <b>Menu > Settings > Repository > Deploy Token</b>. This token was given read/write access to the container registry and read access to the repo, i.e., the variables <code>read_repository</code>, <code>read_registry</code>, and <code>write_registry</code> are set. 
<br/><br/>
In a next step that name, user, token were stored in the file <code>deploy_token.txt</code> in the folder secrets inside repository. This local folder was then immediately added to the <code>.gitignore</code> file, to not push that file to the GitLab server.
<br/><br/>
There is special deploy token for usage CI/CD jobs: Once a deploy token with the special name <code>gitlab-deploy-token</code>is created, it is automatically exposed to the CI/CD jobs in the following two CI/CD variables
<ul>
<li>Its username as <code>CI_DEPLOY_USER</code></li>
<li>Its token as <code>CI_DEPLOY_PASSWORD</code></li>
</ul>
This allows to login in CI/CD jobs to the container registry of the project via 
<br/>
<code>docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY</code>
<br/><br/>
See also the GitLab documentation for more information about Deploy Tokens: https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html

<br/>
<br/>



```yml
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml

# Build a Docker image with CI/CD and push to the GitLab registry.
# Docker-in-Docker documentation: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
#
# This template uses one generic job with conditional builds
# for the default branch and all other (MR) branches.


#------------------------------------------------------------
# Stages
#------------------------------------------------------------
stages:
  - test
  - build

#------------------------------------------------------------
# Global Variables
#------------------------------------------------------------
variables:
  APP_NAME: "django-app"
  TAG_LATEST: "latest"
  IMAGE_TAG_SHA: $CI_REGISTRY_IMAGE/$APP_NAME:$CI_COMMIT_SHORT_SHA
  IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE/$APP_NAME:$TAG_LATEST


# test image build
test:image:build:
  stage: test

  image: tmaier/docker-compose:latest

  services:
    - docker:dind

  before_script:
    - docker info
    - docker-compose --version

  script:
    - docker-compose build

#------------------------------------------------------------
# stage: build
#------------------------------------------------------------
# build job. push to container registry
build-job:      
  stage: build

  image:
    name: docker:latest
  
  services: 
    - docker:dind
  
  before_script:
    # login gitlab registry of project
    - mkdir -p $HOME/.docker/
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_DEPLOY_USER}:${CI_DEPLOY_PASSWORD} | base64 | tr -d '\n')\"}}}" > $HOME/.docker/config.json

  script:
    # build and push a new image  
    - docker build -t $IMAGE_TAG_SHA .
    - docker push $IMAGE_TAG_SHA
    - docker image tag $IMAGE_TAG_SHA $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_LATEST


```   

## Django Database configuration

```py
DATABASES = {
    'default': {
        'ENGINE': 'djongo',
        'ENFORCE_SCHEMA': False,
        'NAME': 'DBIndexer',
        'HOST': 'mongodby',
        'PORT': 27017,
        'USER':  'root',
        'PASSWORD': 'mongoadmin',
        'CLIENT': {
                'host': 'mongodby://mongodby:27017',
                'username': 'root',
                'password': 'mongoadmin',


        }
    }
}
```
## Usage

Run services in the background:
`docker-compose up -d`

Run services in the foreground:
`docker-compose up --build`

Inspect volume:
`docker volume ls`
and
`docker volume inspect <volume name>`

Prune unused volumes:
`docker volume prune`

View networks:
`docker network ls`

Bring services down:
`docker-compose down`

Open a bash session in a running container:
`docker exec -it <container ID> /bin/bash`

